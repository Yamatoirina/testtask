package task_2;

import java.util.Collection;

public class GenericMethod {

    <T> void arrayToCollection(T[] array, Collection<T> col) {
        for (int i = 0; i < array.length; i++) {
            col.add(array[i]);
        }
    }
}
