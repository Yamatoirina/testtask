package task_3;

import java.util.concurrent.atomic.AtomicInteger;

public class IncrementSynchronize {
    private int value = 0;
    //getNextValue()

    public synchronized int getNextValue() {
        return value++;
    }


    public int getNextValue2() {
        synchronized(this){
            return value++;
        }

    }

    public  int getNextValue3() {
        return new AtomicInteger(value).incrementAndGet();
    }
}
