package task_1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class FindWords {
    private static final String file = "C:\\Users\\admin_\\IdeaProjects\\testtask\\src\\main\\java\\task_1\\text.txt";

    public static void main(String[] args) {
        BufferedReader br = null;

        try{
            br = new BufferedReader(new FileReader(file));
            String textFile = null;
            List<String> words = new ArrayList<String>();
            int count = 0;

            while ((textFile = br.readLine())!=null) {
                String[] AllWords = textFile.toLowerCase().split(" |,|\\.|/|!|-|\\?|:|;");
                for (int i = 0; i < AllWords.length; i++) {
                    words.add(AllWords[i]);
                    count++;
                }
            }
            System.out.println("A number of distinct words: "+ count);
            for (String str : words) {
                System.out.println(str);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

}
